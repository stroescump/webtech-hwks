
const FIRST_NAME = "Stroescu";
const LAST_NAME = "Marius";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) {
    //mi-a bubuit creierul incercand toate variantele de operatii aritmetice pt ultima cerinta fml.
    var random=2;
    if(value===Number.POSITIVE_INFINITY || value===Number.NEGATIVE_INFINITY) return NaN;
    else if(isNaN(value)==true) return NaN;
    else if(value>Number.MAX_SAFE_INTEGER || value<Number.MIN_SAFE_INTEGER) return NaN;
    else if(value===Number.MAX_SAFE_INTEGER) return parseInt(value);
    else if(value===Number.MIN_SAFE_INTEGER) return parseInt(value);
    else if(value>0) return parseInt(value);
    
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

